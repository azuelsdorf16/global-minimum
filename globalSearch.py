#Andrew W. Zuelsdorf
#10 November 2014
#CMSC 455
#Project 1

#Tries to find the global minimum's rough area.
def globalSearch(g, xInterval, yInterval, steps=1000):
    #bounds checking
    if xInterval[0] >= xInterval[1]:
        errorMsg = '\'xInterval\' must have the form [low_value, higher_value]!'
        errorMsg += 'You gave: [' + str(xInterval[0]) + ', ' + str(xInterval[1]) + ']'
        raise ValueError(errorMsg)
    elif yInterval[0] >= yInterval[1]:
        errorMsg = '\'yInterval\' must have the form [low_value, higher_value]!'
        errorMsg += 'You gave: [' + str(xInterval[0]) + ', ' + str(xInterval[1]) + ']'
        raise ValueError(errorMsg)
    else:
        pass

    dY = dX = 1.0/1000

    #Impossible values that will be overwritten.
    zMin = False
    xMin = False
    yMin = False

    x = xInterval[0]
    while x <= xInterval[1]:
        y = yInterval[0]
        while y <= yInterval[1]:
            z = g(x, y)
            if zMin == False or zMin > z:
                zMin = z
                xMin = x
                yMin = y
            y += dY
        x += dX

    return xMin, yMin, zMin
