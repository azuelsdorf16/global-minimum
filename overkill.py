#Andrew W. Zuelsdorf
#10 November 2014
#Project 1
#CMSC 455

from math import *
import sys
from miner import findMin

PRECISION = 1000

def f(x, y):
    return exp(sin(50*x))+sin(60*exp(y))+sin(80*sin(x))+sin(sin(70*y))-sin(10*(x+y))+(x*x+y*y)/4.0

#This function finds the local minimum of the function on many very small intervals
#and prints the smallest x, y, z value that it finds. This program is much slower
#but more likely to succeed at finding the local minimum than other programs are.
#It is recommended that one use this function primarily for checking the results of a
#faster global minimum-finding program.
def overkill(g, xInterval, yInterval, steps=1000):
    #bounds checking
    if xInterval[0] >= xInterval[1]:
        errorMsg = '\'xInterval\' must have the form [low_value, higher_value]!'
        errorMsg += 'You gave: [' + str(xInterval[0]) + ', ' + str(xInterval[1]) + ']'
        raise ValueError(errorMsg)
    elif yInterval[0] >= yInterval[1]:
        errorMsg = '\'yInterval\' must have the form [low_value, higher_value]!'
        errorMsg += 'You gave: [' + str(xInterval[0]) + ', ' + str(xInterval[1]) + ']'
        raise ValueError(errorMsg)
    else:
        pass

    zMin = False
    xMin = False
    yMin = False

    DY = 1. / steps
    DX = 1. / steps

    x = xInterval[0]
    while x <= xInterval[1] - DX:
        y = yInterval[0]
        while y <= yInterval[1] - DY:
            x0, y0, z0 = findMin(function=g, xInterval=[x, x + DX], yInterval=[y, y + DY], decPlaces=PRECISION)
            if zMin == False or zMin > z0:
                zMin = z0
                yMin = y0
                xMin = x0
            y += DY 
        x += DX

    print 'x = ' + str(xMin) + ',\ny =' + str(yMin) + ',\nz = ' + str(zMin)

def main():
    overkill(f, [-1, 1], [-1, 1])

if __name__ == "__main__":
    main()
