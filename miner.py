#Andrew W. Zuelsdorf
#Project 1 CMSC 455
#10 November 2014

from mpmath import *

#Test function
def f(x, y):
    return -(x + y)

#Test function
def g(x, y):
    return x ** 2 + y ** 2

def b(x, y):
    return cos(x) + sin(y)

#Test function
def h(x, y):
    return x ** 3 + y ** 3

def p(x, y):
    return abs(x) + abs(y)

def j(x, y):
    return exp(sin(50*x))+sin(60*exp(y))+sin(80*sin(x))+sin(sin(70*y))-sin(10*(x+y))+(x*x+y*y)/4.0

#Finds the local minimum a function on interval x = [xInterval],
#y = [yInterval]. This function assumes that the function has only
#one minimum on the given interval.
def findMin(function, xInterval, yInterval, decPlaces=24):
    #Invalid number of decimal digits. 
    if decPlaces <= 0:
        errorMsg = 'Needed positive number of decimal places, received '
        errorMsg += str(decPlaces)
        raise ValueError(errorMsg)
    #We need an INTEGER for the number of decimal places.
    if type(decPlaces) != type(9):
        errorMsg = 'Needed positive number of decimal places, received '
        errorMsg += str(decPlaces)
        raise ValueError(errorMsg)
    #bounds checking for the x interval
    if xInterval[0] >= xInterval[1]:
        errorMsg = 'lower bound of x interval is greater than '
        errorMsg += 'upper bound of x interval'
        raise ValueError(errorMsg)
    #bounds checking for the y interval
    if yInterval[0] >= yInterval[1]:
        errorMsg = 'lower bound of y interval is greater than '
        errorMsg += 'upper bound of y interval'
        raise ValueError(errorMsg)

    mp.dps = decPlaces + 1 #+1 because we cannot have garbage values
    #in our last decimal place of accuracy.
    
    oldMin = 0
    newMin = 9 #These initial values ensure that the loop
    #does not end before it begins. They will be overwritten
    #in the first and subsequent iterations of the loop.

    dy = (mp.mpf(yInterval[1]) - mp.mpf(yInterval[0])) / 2
    #These dx, dy values allow us to ensure that we test 
    #the end points of the function.
    dx = (mp.mpf(xInterval[1]) - mp.mpf(xInterval[0])) / 2
    x = mp.mpf(xInterval[0] + dx) #Our initial x is in the middle of the x interval
    #and our initial y is in the middle of the y interval
    y = mp.mpf(yInterval[0] + dy)

    #keep going until our minimum is accurate to decPlaces decimal places.
    while abs(newMin - oldMin) > 10 ** (-(decPlaces + 1)):
        #Look in all eight possible directions and look at where we are now.
        #The min and max are there to ensure that x and y stay within their
        #respective intervals.
        case3 = function(x, y)

        case2 = function(x, max(mp.mpf(y - dy), mp.mpf(yInterval[0])))

        case1 = function(x, min(mp.mpf(y + dy), mp.mpf(yInterval[1])))

        case4 = function(min(mp.mpf(x + dx), mp.mpf(xInterval[1])), y)

        case5 = function(max(mp.mpf(x - dx), mp.mpf(xInterval[0])), y)

        case6 = function(min(mp.mpf(x + dx), mp.mpf(xInterval[1])), min(mp.mpf(y + dy), mp.mpf(yInterval[1])))

        case7 = function(max(mp.mpf(x - dx), mp.mpf(xInterval[0])), max(mp.mpf(y - dy), mp.mpf(yInterval[0])))

        case8 = function(min(mp.mpf(x + dx), mp.mpf(xInterval[1])), max(mp.mpf(y - dy), mp.mpf(yInterval[0])))

        case9 = function(max(mp.mpf(x - dx), mp.mpf(xInterval[0])), min(mp.mpf(y + dy), mp.mpf(yInterval[1])))

        #put the new minimum into the old minimum before we overwrite new minimum.
        oldMin = newMin

        #our new minimum
        newMin = min(case1, case2, case3, case4, case5, case6, case7, case8, case9)

        #Move in the direction of the new minimum.
        if case1 == newMin:
            y = min(mp.mpf(y + dy), mp.mpf(yInterval[1]))
        elif case2 == newMin:
            y = max(mp.mpf(y - dy), mp.mpf(yInterval[0]))
        elif case4 == newMin:
            x = min(mp.mpf(x + dx), mp.mpf(xInterval[1]))
        elif case5 == newMin:
            x = max(mp.mpf(x - dx), mp.mpf(xInterval[0]))
        elif case6 == newMin:
            x = min(mp.mpf(x + dx), mp.mpf(xInterval[1]))
            y = min(mp.mpf(y + dy), mp.mpf(yInterval[1]))
        elif case7 == newMin:
            x = max(mp.mpf(x - dx), mp.mpf(xInterval[0]))
            y = max(mp.mpf(y - dy), mp.mpf(yInterval[0]))
        elif case8 == newMin:
            x = min(mp.mpf(x + dx), mp.mpf(xInterval[1]))
            y = max(mp.mpf(y - dy), mp.mpf(yInterval[0]))
        elif case9 == newMin:
            x = max(mp.mpf(x - dx), mp.mpf(xInterval[0]))
            y = min(mp.mpf(y + dy), mp.mpf(yInterval[1]))
        #function(x, y) is our new minimum.
        else:
            pass

        #and decrease dx, dy
        dx /= 2
        dy /= 2

    #Our final x, y, z values accurate to decPlaces decimal places.
    return x, y, newMin

def main():
    pi = 3.14159265359

    #Test functions.
    x, y, z = findMin(f, [0.468, 0.469], [-0.923, -0.92])
    print "x = " + str(x)
    print 'y = ' + str(y)
    print 'z = ' + str(z)
    x, y, z = findMin(g, [0.468, 0.469], [-0.923, -0.92])
    print "x = " + str(x)
    print 'y = ' + str(y)
    print 'z = ' + str(z)
    x, y, z = findMin(p, [-100, 100], [-100, 100])
    print "x = " + str(x)
    print 'y = ' + str(y)
    print 'z = ' + str(z)
    x, y, z = findMin(b, [0, 2*pi], [0, 2*pi])
    print "x = " + str(x)
    print 'y = ' + str(y)
    print 'z = ' + str(z)
    x, y, z = findMin(h, [-1, 1], [-1, 1], 9)
    print "x = " + str(x)
    print 'y = ' + str(y)
    print 'z = ' + str(z)

    for i in range(1, 50, 1):
        mp.dps = i + 1
        x, y, z = findMin(j, [0.468, 0.469], [-0.923, -0.922], i)
        print "x = " + str(x) + ', y = ' + str(y) + ', z = ' + str(z)

if __name__ == "__main__":
    main()
