#Andrew W. Zuelsdorf
#10 November 2014
#CMSC 455
#Project 1
#This file finds the minimum of the function 
#f(x, y) = exp(sin(50x)) + sin(60exp(y)) + sin(80sin(x))+
#sin(sin(70y)) - sin(10(x + y)) + (x^2 + y^2) / 4. It uses
#the globalSearch() function from globalSearch.py to locate
#a very small area that the global minimum resides in.
#It then uses the findMin() function from miner.py to find
#the minimum in this small area.

import sys
from mpmath import mp
import miner
import globalSearch
import math

#Our constants
PRECISION = 1000
xInterval = [-1, 1] #The interval of x values that we are interested in
yInterval = [-1, 1] #The interval of y values that we are interested in
dx = 0.001
dy = 0.001

#The function whose global minimum we must find. Used when precision is less important
#than speed, such as in globalSearch.globalSearch()
def f(x, y):
    return math.exp(math.sin(50 * x)) + math.sin(60 * math.exp(y)) + math.sin(80 * math.sin(x)) + math.sin(math.sin(70 * y)) - math.sin(10 * (x + y)) + (x * x + y * y) / 4.0

#multiple-precision version of f(x, y). Used when precision is
#needed, such as in miner.findMin()
def mpF(x, y):
    return mp.exp(mp.sin(50 * x)) + mp.sin(60 * mp.exp(y)) + mp.sin(80 * mp.sin(x)) + mp.sin(mp.sin(70 * y)) - mp.sin(10 * (x + y)) + (x * x + y * y) / 4.0

def main():
    x = 0
    y = 0
    z = 0

    #Give us a good initial guess for the minimum's x, y, and z = f(x, y) values.
    x, y, z = globalSearch.globalSearch(f, xInterval, yInterval)

    #Get the minimum in a very tiny area around our initial guess 
    minX, minY, minZ = miner.findMin(mpF, xInterval=[x, x + dx], yInterval=[y, y + dy], decPlaces=PRECISION)

    #Get the minimum in a different but still very tiny area around our initial guess 
    newX, newY, newZ = miner.findMin(mpF, xInterval=[x, x + dx], yInterval=[y - dy, y], decPlaces=PRECISION)

    #Is this minimum less than our current value for the minimum?
    if minZ > newZ:
        minX = newX
        minY = newY
        minZ = newZ
   
    #Get the minimum in a different but still very tiny area around our initial guess 
    newX, newY, newZ = miner.findMin(mpF, xInterval=[x - dx, x], yInterval=[y, y + dy], decPlaces=PRECISION)

    #Is this minimum less than our current value for the minimum?
    if minZ > newZ:
        minX = newX
        minY = newY
        minZ = newZ
   
    #Get the minimum in a different but still very tiny area around our initial guess 
    newX, newY, newZ = miner.findMin(mpF, xInterval=[x - dx, x], yInterval=[y - dy, y], decPlaces=PRECISION)

    #Is this minimum less than our current value for the minimum?
    if minZ > newZ:
        minX = newX
        minY = newY
        minZ = newZ
   
    #Print out the fruits of our labor 
    print "Our final x min was x = " + str(minX) + ","
    print "our final y min was y = " + str(minY) + ","
    print "and our final z min was z = " + str(minZ)
    print '--------------------------------------------'

if __name__ == "__main__":
    main()
